import Vue from 'vue'
import Unicon from 'vue-unicons'
import {
  uniCommentAltLock,
  uniCommentRedo,
  uniCommentPlus,
  uniEnvelopeAlt,
  uniUser,
  uniBars,
  uniBell,
  uniUserSquare,
  uniTrophy,
  uniDashboard,
  uniCloudDownload,
  uniExclamationCircle,
  uniBed,
  uniSadCrying,
  uniCommentAltHeart,
  uniCommentAltLines,
  uniBookReader,
  uniPlus,
  uniTrashAlt,
  uniSignOutAlt,
  uniKeyholeCircle,
  uniBooks,
  uniCommentDots,
  uniUsersAlt
} from 'vue-unicons/src/icons'

Unicon.add([
  uniCommentAltLock,
  uniCommentRedo,
  uniCommentPlus,
  uniEnvelopeAlt,
  uniUser,
  uniBars,
  uniBell,
  uniUserSquare,
  uniTrophy,
  uniDashboard,
  uniCloudDownload,
  uniExclamationCircle,
  uniBed,
  uniSadCrying,
  uniCommentAltHeart,
  uniCommentAltLines,
  uniBookReader,
  uniPlus,
  uniTrashAlt,
  uniSignOutAlt,
  uniKeyholeCircle,
  uniBooks,
  uniCommentDots,
  uniUsersAlt
])

export default () => {
  Vue.use(Unicon)
}
