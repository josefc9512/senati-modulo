export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Calificación senati',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Oswald:400,500,600,700|Roboto:400,500,700|Open+Sans:400,600,700,800&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css'
      }
    ],
    script: [
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.6/js/uikit.min.js',
        type: 'text/javascript',
        async: true
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.6/js/uikit-icons.min.js',
        type: 'text/javascript',
        async: true
      }
    ],
    bodyAttrs: {
      class: 'page-learning-template'
    },
    htmlAttrs: {
      lang: 'es',
      amp: true
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  // router: {
  //   middleware: ['auth']
  // },
  css: [
    '~/css/plugins.css',
    '~/css/master.css',
    '~/css/fontawesome/css/all.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '@/plugins/vue-unicons', ssr: false },
    { src: '@/plugins/plyrvideo', ssr: false },
    { src: '@/plugins/toggle-button', ssr: false },
    { src: '@/plugins/vue-countdown', ssr: false },
    { src: '@/plugins/global-events', ssr: false },
    { src: '@/plugins/element-video-upload', ssr: false },
    { src: '@/plugins/vue-pell-editor', ssr: false },
    { src: '@/plugins/vue-data-tables', ssr: false }
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    [
      '@nuxtjs/component-cache',
      {
        max: 10000,
        maxAge: 1000 * 60 * 60
      }
    ]
  ],
  "config": {
    "nuxt": {
      "host": "0.0.0.0",
      "port": "8011"
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: 'http://192.168.8.125/senati-api/api'
  },
  /*
   ** Build configuration
   */
  build: {
    transpile: [/^element-ui/],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  auth: {
    redirect: {
      logout: '/login',
      callback: '/login',
      home: '/calification'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            baseURL: 'http://192.168.8.125/senati-api',
            url: '/api/usuario/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: {
            baseURL: 'http://192.168.8.125/senati-api',
            url: '/api/usuario/logout',
            method: 'post'
          },
          user: {
            baseURL: 'http://192.168.8.125/senati-api',
            url: '/api/usuario/user',
            method: 'get',
            propertyName: 'user'
          }
        }
      }
    }
  }
}
